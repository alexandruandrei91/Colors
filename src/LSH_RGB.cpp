#include "LSH_RGB.h"

#include "RGB.h"

#include <algorithm>

auto LSH_RGB::convert(LSH const& lsh) -> RGB
{
    return apply_luminosity( lsh.l
                           , apply_saturation( lsh.s
                                             , apply_hue(lsh.h)));
}

auto LSH_RGB::apply_luminosity(decltype(LSH::l) const l, RGB const& rgb) -> RGB
{
    auto const report = (double)RGB::MAX_COL / LSH::MAX_L;
    auto const lum = l * report - RGB::MAX_COL / 2;
    return { static_cast<decltype(RGB::r)>(rgb.r + lum)
           , static_cast<decltype(RGB::g)>(rgb.g + lum)
           , static_cast<decltype(RGB::b)>(rgb.b + lum) };
}

auto LSH_RGB::apply_saturation(decltype(LSH::s) const s, RGB const& rgb) -> RGB
{
    auto const _s = static_cast<float>(s) / LSH::MAX_S;

    auto const [min_c, max_c] = std::minmax({rgb.r, rgb.g, rgb.b});
    auto const med_c = (min_c + max_c + 1) / 2;

    auto helper =
        [med_c, _s](auto const c) -> decltype(c)
        {
            return med_c - _s * (med_c - c);
        };

    return { helper(rgb.r)
           , helper(rgb.g)
           , helper(rgb.b) };
}

auto LSH_RGB::apply_hue(decltype(LSH::h) const h) -> RGB
{
    static auto const MAX_REGION = LSH::MAX_H / 6;

    decltype(RGB::r) const asc = (h % MAX_REGION) * (LSH::MAX_COL / MAX_REGION);
    decltype(RGB::r) const desc = LSH::MAX_COL - asc - 1;

    auto const region = (h % LSH::MAX_H) / MAX_REGION;
    auto const max_c = RGB::MAX_COL - 1;

    switch (region)
    {
    case 0:
        return {max_c, asc, 0};
    case 1:
        return {desc, max_c, 0};
    case 2:
        return {0, max_c, asc};
    case 3:
        return {0, desc, max_c};
    case 4:
        return {asc, 0, max_c};
    case 5:
        return {max_c, 0, desc};
    }

    return {0, 0, 0};
}
