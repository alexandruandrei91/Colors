#include "RGB_LSH.h"

#include "RGB.h"

#include <algorithm>
#include <array>


auto RGB_LSH::convert(RGB const& rgb) -> LSH
{
    return { extract_luminosity(rgb)
           , extract_saturation(rgb)
           , extract_hue(rgb) };
}

auto RGB_LSH::extract_luminosity(RGB const& rgb) -> decltype(LSH::l)
{
    auto const [min_c, max_c] = std::minmax({rgb.r, rgb.g, rgb.b});

    auto const report = (double)LSH::MAX_L / RGB::MAX_COL;
    return ((min_c + max_c + 1) / 2) * report;
}

auto RGB_LSH::extract_saturation(RGB const& rgb) -> decltype(LSH::s)
{
    auto const [min_c, max_c] = std::minmax({rgb.r, rgb.g, rgb.b});

    auto const report = (double)LSH::MAX_S / RGB::MAX_COL;
    return (max_c - min_c) * report;
}

auto RGB_LSH::extract_hue(RGB const& rgb) -> decltype(LSH::h)
{
    std::array<decltype(rgb.r), 3> const _rgb {rgb.r, rgb.g, rgb.b};

    auto [min, max] = std::minmax_element(std::begin(_rgb), std::end(_rgb));

    auto const min_i = std::distance(std::begin(_rgb), min);
    auto const max_i = std::distance(std::begin(_rgb), max);
    auto const mid_i = std::size(_rgb) - (min_i + max_i);

    if (*min == *max)
    {
        return 0;
    }
    else if (_rgb[min_i] == _rgb[mid_i])
    {
        return max_i * LSH::MAX_COL;
    }
    else if (_rgb[max_i] == _rgb[mid_i])
    {
        auto const i1 = (mid_i == 0 && max_i == 2)
                      ? 3
                      : mid_i;

        auto const i2 = (max_i == 0 && mid_i == 2)
                      ? 3
                      : max_i;

        return (i1 + i2) * (LSH::MAX_COL / 2);
    }
    else
    {
        auto const start_h = (min_i + 1) % std::size(_rgb) * LSH::MAX_COL + LSH::MAX_COL / 2;

        auto const dir =
            (max_i - mid_i == 1) || (max_i == 0 && mid_i == 2)
            ? 1
            : -1;

        float const _mid = _rgb[mid_i] - _rgb[min_i];
        float const _max = _rgb[max_i] - _rgb[min_i];

        auto const offset = dir * (1 - _mid / _max);

        return start_h + offset * (LSH::MAX_COL/2);
    }
}
