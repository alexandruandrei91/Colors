#include "RGB.h"
#include "LSH.h"
#include "RGB_LSH.h"
#include "LSH_RGB.h"

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <cstdio>
#include <fstream>
#include <string>
#include <vector>


namespace test
{
    auto test_conversions() -> bool
    {
        auto test_conversion =
            [](RGB const & start_rgb) -> bool
            {
                auto lsh = RGB_LSH::convert(start_rgb);
                auto rgb = LSH_RGB::convert(lsh);

                auto const cmp =
                [](auto const a, auto const b)
                {
                    static auto const eps = 1;
                    return std::abs(a - b) <= eps;
                };

                if (cmp(start_rgb.r, rgb.r)
                    && cmp(start_rgb.g, rgb.g)
                    && cmp(start_rgb.b, rgb.b))
                {
                    return true;
                }

                // printf("Start  RGB: %d, %d, %d\n", start_rgb.r, start_rgb.g, start_rgb.b);
                // printf("RGB -> LSH: %d, %d, %d\n", lsh.l, lsh.s, lsh.h);
                // printf("LSH -> RGB: %d, %d, %d\n", rgb.r, rgb.g, rgb.b);
                // printf("\n");

                return false;
            };

        auto err_count = 0;
        for (int r = 0; r < 256; r++)
            for (int g = 0; g < 256; g++)
                for (int b = 0; b < 256; b++)
                {
                    if (!test_conversion({ static_cast<uint8_t>(r)
                                         , static_cast<uint8_t>(g)
                                         , static_cast<uint8_t>(b) }))
                    {
                        err_count++;
                    }
                }

        printf("%d errors.\n", err_count);

        return err_count == 0;
    }

    struct BMP
    {
#pragma pack(2)
        struct HeaderFile
        {
            uint16_t type;
            uint32_t size;
            uint16_t reserved1;
            uint16_t reserved2;
            uint32_t offset_bits;
        };

#pragma pack(2)
        struct HeaderInfo
        {
            uint32_t size;
            int32_t width;
            int32_t height;
            uint16_t planes;
            uint16_t bit_count;
            uint32_t compression;
            uint32_t size_image;
            int32_t x_pixels_per_meter;
            int32_t y_pixels_per_meter;
            uint32_t clr_used;
            uint32_t clr_important;
        };

        struct BGR
        {
            uint8_t b;
            uint8_t g;
            uint8_t r;
        };

        using buffer_type = std::vector<BGR>;

        HeaderFile header_file;
        HeaderInfo header_info;
        buffer_type buffer;

        BMP(HeaderFile hf, HeaderInfo hi)
            : header_file(hf)
            , header_info(hi)
            , buffer(hi.width * hi.height)
        {}
    };

    auto load_bmp(std::istream & stream) -> BMP
    {
        BMP::HeaderFile hf{};
        stream.read(reinterpret_cast<char *>(&hf), sizeof(hf));

        BMP::HeaderInfo hi{};
        stream.read(reinterpret_cast<char *>(&hi), sizeof(hi));

        BMP bmp {hf, hi};

        stream.read(reinterpret_cast<char *>(std::data(bmp.buffer)), std::size(bmp.buffer) * sizeof(BMP::buffer_type::value_type));

        return bmp;
    }

    auto write_bmp(std::ostream & stream, BMP const & bmp, std::vector<RGB> const & rgb_buff) -> void
    {
        stream.write(reinterpret_cast<char const *>(&bmp.header_file), sizeof(bmp.header_file));
        stream.write(reinterpret_cast<char const *>(&bmp.header_info), sizeof(bmp.header_info));

        stream.write(reinterpret_cast<char const *>(std::data(rgb_buff)), std::size(rgb_buff) * sizeof(RGB));
    }


    template<typename T>
    auto write_buff(std::string const & rgb_path_out, std::vector<T> const & buff) -> bool
    {
        std::fstream file;

        file.open(rgb_path_out, std::ios::out | std::ios::binary | std::ios::trunc);
        if (!file.is_open())
            return false;

        file.write(reinterpret_cast<char const *>(std::data(buff)), std::size(buff) * sizeof(T));

        file.close();

        return true;
    }

    auto test_bmp(std::string const & bmp_path_in, std::string const & bmp_path_out) -> bool
    {
        std::fstream bmp_file;

        bmp_file.open(bmp_path_in, std::ios::in | std::ios::binary);
        if (!bmp_file.is_open())
            return false;

        auto const bmp = load_bmp(bmp_file);
        bmp_file.close();

        bmp_file.open(bmp_path_out, std::ios::out | std::ios::binary | std::ios::trunc);
        if (!bmp_file.is_open())
            return false;

        int min_l = 10000, min_s = 10000, min_h = 10000;
        int max_l = -10000, max_s = -10000, max_h = -10000;
        std::vector<LSH> lsh_buff;
        lsh_buff.reserve(std::size(bmp.buffer));
        std::transform(std::begin(bmp.buffer)
                       , std::end(bmp.buffer)
                       , std::back_inserter(lsh_buff)
                       , [&](BMP::BGR const & bgr) -> LSH
                       {
                           const auto& lsh = RGB_LSH::convert({bgr.b, bgr.g, bgr.r});
                           if (lsh.l < min_l)
                               min_l = lsh.l;
                           if (lsh.s < min_s)
                               min_s = lsh.s;
                           if (lsh.h < min_h)
                               min_h = lsh.h;

                           if (lsh.l > max_l)
                               max_l = lsh.l;
                           if (lsh.s > max_s)
                               max_s = lsh.s;
                           if (lsh.h > max_h)
                               max_h = lsh.h;
                           return lsh;
                       });
        write_buff(bmp_path_in + ".lsh", lsh_buff);

        std::vector<RGB> rgb_converted_buff;
        rgb_converted_buff.reserve(std::size(bmp.buffer));
        std::transform(std::begin(lsh_buff)
                       , std::end(lsh_buff)
                       , std::back_inserter(rgb_converted_buff)
                       , [](LSH const & lsh) -> RGB
                       {
                           return LSH_RGB::convert(lsh);
                       });
        write_buff(bmp_path_in + ".rgb", rgb_converted_buff);

        write_bmp(bmp_file, bmp, rgb_converted_buff);
        bmp_file.close();

        printf("Min L:%d, S:%d, H:%d\n", min_l, min_s, min_h);
        printf("Max L:%d, S:%d, H:%d\n", max_l, max_s, max_h);

        return true;
    }
}

int main()
{

    //test::test_bmp("../res/colors.bmp", "../res/colors_t.bmp");

    test::test_conversions();
    // RGB start_rgb {0, 194, 221};
    // auto lsh = RGB_LSH::convert(start_rgb);
    // auto rgb = LSH_RGB::convert({55, 55, 399});

    // printf("Start  RGB: %d, %d, %d\n", start_rgb.r, start_rgb.g, start_rgb.b);
    // printf("RGB -> LSH: %d, %d, %d\n", lsh.l, lsh.s, lsh.h);
    // printf("LSH -> RGB: %d, %d, %d\n", rgb.r, rgb.g, rgb.b);

    return 0;
}
