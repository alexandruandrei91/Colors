# Colors

## Description
 This is an experiment with a different color format called LSH (Luminance, Saturation, Hue),
similar to HLS. The main goal of the experiment is to have each component of the color easily
compressable due to the redundancy of some information. For example if the luminance (which
represents how close is the color to Black or White) is L MIN or L MAX, the saturation (which
reresents how close is the color to its corresponding gray) is redundant and so is the hue. Or
if the saturation is 0, the hue is not needed anymore. This properties are linear, so if the
saturation is half, we only need half precision for the hue. Hopefully, this would imply a
significant reduction in size of images using this format. Other compression mechanisms could
be applied afterwards.

 This is also a small experiment to test some functional programming concepts in C++.

**NEEDS A LOT OF WORK**
