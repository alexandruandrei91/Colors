#pragma once

#include "LSH.h"

#include <cstdint>


class RGB;

class LSH_RGB
{
public:
    static auto convert(LSH const& lsh) -> RGB;

private:
    static auto apply_luminosity(decltype(LSH::l) const l, RGB const& rgb) -> RGB;
    static auto apply_saturation(decltype(LSH::s) const s, RGB const& rgb) -> RGB;
    static auto apply_hue(decltype(LSH::h) const h) -> RGB;
};
