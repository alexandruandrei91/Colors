#pragma once

#include <cstdint>
#include <limits>


struct RGB
{
    uint8_t r;
    uint8_t g;
    uint8_t b;

    static auto constexpr MAX_COL = 256;

    RGB() = delete;
};
