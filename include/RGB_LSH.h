#pragma once

#include "LSH.h"

#include <cstdint>


class RGB;

class RGB_LSH
{
public:
    static auto convert(RGB const& rgb) -> LSH;

private:
    static auto extract_luminosity(RGB const& rgb) -> decltype(LSH::l);
    static auto extract_saturation(RGB const& rgb) -> decltype(LSH::s);
    static auto extract_hue(RGB const& rgb) -> decltype(LSH::h);
};
